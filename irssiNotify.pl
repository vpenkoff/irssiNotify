#!/usr/bin/perl
use warnings;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi;
use System::Sub 'zenity';

$VERSION = '1.0.0';
%IRSSI = (
  authors => 'Viktor Penkov',
  contact => 'vpenkoff@gmail.com',
  name => 'irssiNotify',
  description => 'Display notifications from irssi ' .
                 'using zenity',
  license     => 'MIT'
);

sub event_privmsg {
  # $data = "nick/#channel :text"
  my ($server, $data, $nick, $address) = @_;
  eval {
    zenity '--notification',
    'timeout=5',
    '--window-icon="info"',
    '--text=' . $nick . ': ' . $data ;
  };
}

Irssi::signal_add("message private", "event_privmsg")
